# vscode

[Visual Studio Code](https://code.visualstudio.com/) の設定や使い方など

## 動作確認環境

- OS : Ubuntu 20.04 LTS

    ```
    $ cat /etc/os-release 
    NAME="Ubuntu"
    VERSION="20.04 LTS (Focal Fossa)"
    ID=ubuntu
    ID_LIKE=debian
    PRETTY_NAME="Ubuntu 20.04 LTS"
    VERSION_ID="20.04"
    HOME_URL="https://www.ubuntu.com/"
    SUPPORT_URL="https://help.ubuntu.com/"
    BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
    PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
    VERSION_CODENAME=focal
    UBUNTU_CODENAME=focal
    ```

- vscode バージョン : 1.47以降

## インストール

- Ubuntuの場合、[公式HP](https://code.visualstudio.com/) から`.deb`をダウンロードしてからインストールしないと、
  日本語入力ができないなどの問題がある。
