# Visual Studio Code Remote Development Extension Pack

- https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack

SSH 接続先や Docker コンテナをエージェントとしてリモート上で VS Code を実行可能にする。

## メモ
- コンテナは `root` ユーザじゃないと色々面倒そう
- extension を一度インストールすると、その情報がどこかに残っているらしく、
  コンテナを消して別のコンテナを起動してもインストール済み状態。
- どうも Docker イメージごとに WorkSpace が共有になっているように見える。
