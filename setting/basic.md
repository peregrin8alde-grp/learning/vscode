# 基本設定

設定画面（`[Ctrl+,]`）から行うが、設定画面で右上の `Open Settings` ボタンを押して `settings.json` を開いて
変更するものだけ直接記述した方が早い。

```
{
    "workbench.startupEditor": "newUntitledFile",
    "editor.minimap.enabled": false,
    "files.enableTrash": false,
    "files.eol": "\n",
    "files.watcherExclude": {
        "**/.git/objects/**": true,
        "**/.git/subtree-cache/**": true,
        "**/node_modules/*/**": true
    },
    "editor.formatOnSave": true,
    "window.openFoldersInNewWindow": "on"
}
```
