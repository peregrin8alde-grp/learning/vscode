# サンプルプロジェクト

コンテナ共有の Maven リポジトリを作成しておく。

```
$ docker volume create --name maven-repo
```


シンプル構成の Maven プロジェクトで開始する。

参考 : 

- https://maven.apache.org/archetype/index.html
- https://maven.apache.org/archetype/maven-archetype-plugin/index.html
- https://maven.apache.org/archetype/maven-archetype-plugin/generate-mojo.html
- https://maven.apache.org/archetypes/
- https://maven.apache.org/archetypes/maven-archetype-simple/
- https://maven.apache.org/archetype/maven-archetype-plugin/examples/generate-batch.html

```
docker run \
  -it --rm \
  -v maven-repo:/root/.m2 \
  -v `pwd`:/sample \
  -w / \
  maven \
    mvn archetype:generate \
      -DinteractiveMode=false \
      -DarchetypeGroupId=org.apache.maven.archetypes \
      -DarchetypeArtifactId=maven-archetype-simple \
      -DarchetypeVersion=1.4 \
      -DgroupId=mygrp \
      -DartifactId=sample \
      -Dversion=1.0-SNAPSHOT \
      -Dpackage=mygrp.sample
```

開発時に他コンテナに接続など行う場合は前もってネットワークを作成して利用。

```
docker network create devcontainer_sample_net
```
